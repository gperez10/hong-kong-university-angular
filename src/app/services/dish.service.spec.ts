import { TestBed } from '@angular/core/testing';

import { dishService } from './dish.service';

describe('dishService', () => {
  let service: dishService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(dishService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
